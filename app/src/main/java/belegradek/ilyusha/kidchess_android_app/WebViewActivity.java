package belegradek.ilyusha.kidchess_android_app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebMessage;
import android.webkit.WebMessagePort;
import android.webkit.WebView;
import android.webkit.WebViewClient;

//this activity contains the web view that hosts the game from the Kid Chess website
public class WebViewActivity extends AppCompatActivity
{
    private WebView game;
    private WebMessagePort port;
    private String url;

    final Handler myHandler = new Handler();



    //initialization method
    @SuppressLint("JavascriptInterface")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        game = findViewById(R.id.gameWebview);
        game.getSettings().setJavaScriptEnabled(true);

        //prevents "play as black" from opening in broswer
        game.setWebViewClient(new WebViewClient());

        //determining which game to load
        String gameChosen = getIntent().getStringExtra("game");
        if(gameChosen.equals("ezchess"))
        {
            //url = "https://www.kidchess.com/MobileApp/dist/ezchess/";
            //game.loadUrl(url);

            url = "https://kidchess.herokuapp.com/dist/ezchess/";
            game.loadUrl(url);
        }
        else if(gameChosen.equals("puzzlejam"))
        {
            url = "https://www.kidchess.com/MobileApp/dist/fast_puzzles/";
            game.loadUrl(url);
        }
        else if(gameChosen.equals("monsterchess"))
        {
            //game.loadUrl("https://www.kidchess.com/MobileApp/dist/monster_chess/");
            url = "https://kidchess.herokuapp.com/dist/monster_chess/";
            game.loadUrl(url);
        }
        else if(gameChosen.equals("checkmateking"))
        {
            //game.loadUrl("https://www.kidchess.com/MobileApp/dist/checkmate_the_king/");
            url = "https://kidchess.herokuapp.com/dist/checkmate_the_king/";
            game.loadUrl(url);
        }

        //used to handle communication from webview
        //to enable disable scrolling
        final JavaScriptInterface myJavaScriptInterface
                = new JavaScriptInterface(this);
        game.addJavascriptInterface(myJavaScriptInterface, "AndroidFunction");

    }

    //class that disables scrolling when user selects a piece
    //and reenables it when they drop it
    //kind of laggy but idk if that can be fixed?
    public class JavaScriptInterface
    {
        Context mContext;

        public JavaScriptInterface(Context c)
        {
            mContext = c;
        }

        @JavascriptInterface
        public void enableScroll()
        {
            myHandler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    game.setOnTouchListener(new View.OnTouchListener()
                    {
                        @Override
                        public boolean onTouch(View v, MotionEvent event)
                        {
                            return false;
                        }
                    });
                }
            });
        }

        @JavascriptInterface
        public void disableScroll()
        {
            myHandler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    game.setOnTouchListener(new View.OnTouchListener(){
                        @Override
                        public boolean onTouch(View v, MotionEvent event){
                            return (event.getAction() == MotionEvent.ACTION_MOVE);
                        }
                    });
                }
            });
        }
    }
}
