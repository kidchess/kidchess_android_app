package belegradek.ilyusha.kidchess_android_app;

import java.util.List;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

//used to actually create the recycler view
//based on this tutorial - https://www.vogella.com/tutorials/AndroidRecyclerView/article.html
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>
{
    private List<String> values;

    private ProgressBar progressBar;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView gameName;
        public TextView gameDescription;
        public ImageView gameImage;
        public View layout;

        public ViewHolder(View v)
        {
            super(v);
            layout = v;
            gameName = v.findViewById(R.id.gameName);
            gameDescription = v.findViewById(R.id.gameDescription);
            gameImage = v.findViewById(R.id.gameImage);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(List<String> myDataset)
    {
        values = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType)
    {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v;

        int screenSize = parent.getResources().getConfiguration().screenLayout &
            Configuration.SCREENLAYOUT_SIZE_MASK;
        //System.out.println(screenSize);
        if(screenSize > 2)
        {
            v = inflater.inflate(R.layout.row_layout_large, parent, false);
        }
        else
        {
            v = inflater.inflate(R.layout.row_layout, parent, false);
        }

        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        String[] descriptions = {"Practice piece movement by playing a real game of chess against our EZChess computer!",
                "Solve as many puzzles as you can before the clock runs out! Don't make too many mistakes!",
                "Learn how the pieces move by playing Monster Chess. Try and beat your own best time!",
                "Practice different forms of check-mate now. Try and beat your own best time!"};
        int[] images = {R.drawable.ezchessimage, R.drawable.puzzlejamimage, R.drawable.monsterchessimage, R.drawable.checkmateimage};

        // - get element from your data set at this position
        // - replace the contents of the view with that element
        final String name = values.get(position);
        holder.gameName.setText(name);
        holder.gameDescription.setText(descriptions[position]);
        holder.gameImage.setImageResource(images[position]);
        //determines what happens when user clicks on item
        //navigate to game
        holder.gameName.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                MainActivity.progressBar.setVisibility(View.VISIBLE);
                Intent intent = new Intent(v.getContext(), WebViewActivity.class);
                if(position == 0)
                    intent.putExtra("game", "ezchess");
                else if(position == 1)
                    intent.putExtra("game", "puzzlejam");
                else if(position == 2)
                    intent.putExtra("game", "monsterchess");
                else if(position == 3)
                    intent.putExtra("game", "checkmateking");
                v.getContext().startActivity(intent);
            }
        });
        holder.gameDescription.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                MainActivity.progressBar.setVisibility(View.VISIBLE);
                Intent intent = new Intent(v.getContext(), WebViewActivity.class);
                if(position == 0)
                    intent.putExtra("game", "ezchess");
                else if(position == 1)
                    intent.putExtra("game", "puzzlejam");
                else if(position == 2)
                    intent.putExtra("game", "monsterchess");
                else if(position == 3)
                    intent.putExtra("game", "checkmateking");
                v.getContext().startActivity(intent);
            }
        });
        holder.gameImage.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                MainActivity.progressBar.setVisibility(View.VISIBLE);
                Intent intent = new Intent(v.getContext(), WebViewActivity.class);
                if(position == 0)
                    intent.putExtra("game", "ezchess");
                else if(position == 1)
                    intent.putExtra("game", "puzzlejam");
                else if(position == 2)
                    intent.putExtra("game", "monsterchess");
                else if(position == 3)
                    intent.putExtra("game", "checkmateking");
                v.getContext().startActivity(intent);
            }
        });

        //holder.gameName.setText(name);
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
        return values.size();
    }

}
