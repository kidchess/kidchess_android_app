package belegradek.ilyusha.kidchess_android_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.webkit.WebView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

/**
 * starting class
 * contains list view with playable games
 * and action bar with hamburger menu of playable games
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{
    //
    //instance variables
    //

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private List<String> games = new ArrayList<>();

    public static ProgressBar progressBar;

    //
    //methods
    //

    //initialization method
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //default stuff
        //setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        //recycler view stuff
        //creates the list of games

        recyclerView = (RecyclerView) findViewById(R.id.gamesRecyclerView);
        recyclerView.setHasFixedSize(true); //helps optimization apparently

        //makes it look like a list
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //adds the items to the list
        games.add("EzChess");
        games.add("Puzzle Jam!");
        games.add("Monster Chess");
        games.add("Checkmate the King");
        mAdapter = new MyAdapter(games);
        recyclerView.setAdapter(mAdapter);

        //loading bar stuff
        //used to indicate loading when opening webview
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
    }

    //determines what back button does
    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) //if the hamburger menu is open, close it
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else //exit the app
        {
            super.onBackPressed();
        }
    }

    //opens the hamburger menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    //navigation for hamburger menu
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        //user chose ezchess
        if (id == R.id.hamburger_ezchess)
        {
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra("game", "ezchess");
            startActivity(intent);
            progressBar.setVisibility(View.VISIBLE);
        }
        else if (id == R.id.hamburger_puzzlejam) //user chose puzzle jam
        {
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra("game", "puzzlejam");
            startActivity(intent);
            progressBar.setVisibility(View.VISIBLE);
        }
        else if (id == R.id.hamburger_monsterchess) //monster chess
        {
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra("game", "monsterchess");
            startActivity(intent);
            progressBar.setVisibility(View.VISIBLE);
        }
        else if (id == R.id.hamburger_checkmateking) //checkmate the king
        {
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra("game", "checkmateking");
            startActivity(intent);
            progressBar.setVisibility(View.VISIBLE);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
}
